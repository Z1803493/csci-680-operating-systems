/****************************************************************
CSCI 480/580 - Assignment 5 - Semester (Fall) 2016
Program:        schedule.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Sunday, October 23, 2016
Purpose:        Header file to implement the scheduling 
****************************************************************/
#ifndef PROG_H
#define PROG_H

#include <iostream>
#include <queue>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;
const char DEL = ' ';
const int MAX_TIME = 500;
const int AT_ONCE = 5;
const int QUEUE_SIZE = 20;
const int HOW_OFTEN = 25;


class Process{
	//taking public to access variables from main 
	public:
	
	string processName;
	static int next_id;
	int ppriority, processId, ArrivalTime, 
	CPUTimer, IOTimer, CPUTotal, ITotal, 
	OTotal, CPUCount, ICount, OCount, sub, waitingtime;
	vector < pair < char , int > > history;

//	public:
//	friend void status(const int)
	//char getfirst(){
	//	return history.first;	
	//}

	//int getsecond(){
	//	return history.second;
	//}
	int getCPUTimer(){
		return CPUTimer;
	}
	void setCPUTimer(){
		CPUTimer = CPUTimer + 1;
	}
	int getIOTimer(){
		return IOTimer;
	}
	int getCPUTotal(){
		return CPUTotal;
	}
	int getITotal(){
		return ITotal;
	}
	int getOTotal(){
		return OTotal;
	}
	int getCPUCount(){
		return CPUCount;
	}
	int getICount(){
		return ICount;
	}
	int getOCount(){
		return OCount;
	}
	
	int getpriority(){
		return ppriority;
	}
	int getprocessId(){
		return processId;
	}
	int getarrivaltime(){
		return ArrivalTime;
	}
	string	getprocessName(){
		return processName;
	}
	void setprocessName(string line){
		stringstream ss(line),strtoint; // Turn the string into a stream.
	  	string tok;
	  	getline(ss, tok, DEL);
	  	if (tok=="STOPHERE")
	  		return;
		this->processName = tok;
		for (int i = 0; i < 2; ++i){
			getline(ss, tok, DEL);
			strtoint.clear();
			strtoint.str(tok);
			strtoint>>this->ppriority;
		}
		for (int i = 0; i < 3; ++i){
			getline(ss, tok, DEL);
		  	strtoint.clear();
			strtoint.str(tok);
			strtoint>>this->ArrivalTime;
		}
	}
	void sethistory(string line){
		stringstream ss(line); // Turn the string into a stream.
	  	string tok;
	  	while(!ss.eof()) {
	    pair<char, int> flag;
	    ss>>flag.first;
	    ss>>flag.second;
	    history.push_back(flag);
	  }
	}
	string gethistory(){
		string lamo;
		stringstream s(lamo);

		for (int i = 0; i < history.size(); ++i){
			s.str("");
			s<<history[i].first;
			lamo = lamo + s.str() + " ";
			s.str("");
			s<<history[i].second;
			lamo = lamo + s.str() + " ";
		}
		return lamo;
	}
	//constructor
	Process():processId(next_id++){
	processName = "";
	ArrivalTime = sub = waitingtime=CPUTimer = IOTimer = CPUTotal = ITotal = OTotal = CPUCount = ICount = OCount =0;

		
	}
	~Process(){
		
	
	}
		
};

//autoincriment proces id
int Process::next_id = 101;

Process* Active = NULL;
Process* IActive = NULL;
Process* OActive = NULL;

//CLASS for implementing priority queue
class Compare{
public:
	bool operator()(Process* l, Process* r){
		return l-> getpriority() < r-> getpriority();
	}
};
//method to show the entry queue
void showpidentry (const queue<Process*> eq);

//method to show the process if of the given priority queue
void showpid (const priority_queue<Process*,deque<Process*>, Compare > q);

#endif