/****************************************************************
CSCI 480/580 - Assignment 5 - Semester (Fall) 2016
Program:        schedule.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Sunday, October 23, 2016
Purpose:        Header file to implement the scheduling 
****************************************************************/

#include "schedule.h"

int main()
{
	int i =0 ;
	ifstream infile("data5.txt");
	string line;
	Process p[12];

	while (infile.good() && i<12){
		getline(infile,line);
		p[i].setprocessName(line);
		getline(infile,line);
		p[i].sethistory(line);
		i++;
	}

	queue<Process*> entryqueue;
	priority_queue<Process*,deque<Process*>, Compare > readyqueue;
	priority_queue<Process*,deque<Process*>, Compare > inputqueue;
	priority_queue<Process*,deque<Process*>, Compare > outputqueue;
	
	//copy all processes to entry queue
	for (int i = 0; i < 12; ++i)
		entryqueue.push(&p[i]);
	
	int ProcessCounter,terminatedcounter,idlecounter = 0;

	for ( i = 0; i < MAX_TIME; ++i){
		if ( entryqueue.empty() && readyqueue.empty() && inputqueue.empty() && outputqueue.empty() && Active == NULL && IActive == NULL && OActive == NULL){
			break;
		}
		//move process from entry to ready queue
		while(!entryqueue.empty() && ProcessCounter < AT_ONCE  ){
			Process* temp = entryqueue.front();
			if ( temp-> ArrivalTime > i )
					break;
			cout<<"Process "<<temp->processId<<" moved from the Entry Queue into the Ready Queue at time "<< i <<endl<<endl;
			readyqueue.push(temp);
			entryqueue.pop();
			ProcessCounter++;
		}
		//printing the status at the time multiple of the 25
		if(i%HOW_OFTEN== 0){
			cout<<"Status at time "<<i<<endl;
			if (Active == NULL)
				cout<<"Active is 0"<<endl;
			else
				cout<<"Active is "<<Active-> processId<<endl;
			if (IActive == NULL)
				cout<<"IActive is 0"<<endl;
			else
				cout<<"IActive is "<<IActive->processId<<endl;
			if (OActive == NULL)
				cout<<"OActive is 0"<<endl;
			else
				cout<<"OActive is "<<OActive->processId<<endl;
			cout<<"Contents of the Entry Queue:"<<endl;
			showpidentry(entryqueue);    
			cout<<"Contents of the Ready Queue:"<<endl;
			showpid(readyqueue);  
			cout<<"Contents of the Input Queue:"<<endl;
			showpid(inputqueue);
			cout<<"Contents of the Output Queue:"<<endl;
			showpid(inputqueue);
		}
		// to check for active ,i active, o active states 
		if ( Active == NULL )
		{
			//check for the ready queue and also push the high priority to active queue
			if (!readyqueue.empty()){
				Active = readyqueue.top();
				readyqueue.pop();
				Active -> CPUCount ++;
			}
			else{
				//idle state 
				idlecounter++;
				cout<<"At time "<<i<<", Active is 0, so we have idle time for a while"<<endl<<endl;
			}
		}
		//as active is not null it has to identify the pair and make it to status and remove from queues 
		if ( Active != NULL )
		{
			Active-> CPUTimer ++;
			Active-> CPUTotal ++;
			if (Active->CPUTimer > Active -> history[Active -> sub].second)
			{
				Active -> CPUTimer   = 0;
				Active -> sub ++;
				//to input queue
				if ( Active -> history[Active -> sub].first == 'I'){
					inputqueue.push(Active);
					Active = NULL;

				}
				//to output queue
				else if ( Active -> history[Active -> sub].first == 'O'){
					outputqueue.push(Active);
					Active = NULL;
				}
				//exiting the process
				else if ( Active -> history[Active -> sub].first == 'N'){
					Active -> waitingtime = i -  Active -> ArrivalTime - Active -> CPUTotal - Active -> ITotal - Active -> OTotal;
					//Status of the process
					cout<<"Process "<<Active->processId<<" has ended."<<endl
					<<"Name:  "<<Active->processName<<endl
					<<"Started at time "<<Active -> ArrivalTime<<" and ended at time "<< i<<endl
					<<"Total CPU time = "<< Active -> CPUTotal <<" in "<< Active -> CPUCount<<" CPU bursts"<<endl
					<<"Total Input time =  "<<Active -> ITotal<<" in "<<Active -> ICount<<" Input bursts"<<endl
					<<"Total Output time =  "<< Active -> OTotal<<" in "<< Active -> OCount<<" Output bursts"<<endl
					<<"Time spent in waiting:  "<< Active -> waitingtime<<endl<<endl;	
					Active = NULL;
					ProcessCounter--;
					terminatedcounter++;					
				}
			}

		}
		//chekcing for the input process of no process is there then pull item from inputqueue
		if ( IActive ==  NULL){
			if (!inputqueue.empty()){
				IActive = inputqueue.top();
				inputqueue.pop();	
				IActive->ICount ++;
			}
		}
		//if we have items in input acgtive state have to increment the variables 
		if ( IActive != NULL){
			if (IActive->IOTimer < IActive->history[IActive->sub].second)
			{
			       IActive-> IOTimer ++;
			       IActive-> ITotal ++;

			}
			else
			{
			       IActive -> IOTimer   = 0;
			       IActive -> sub ++ ;
			       readyqueue.push(IActive);
			       IActive = NULL;
			}
		}
		//if the output pointer is empty then push from the priority queue to the main queue 
		if ( OActive ==  NULL){
			if (!outputqueue.empty()){
				OActive = outputqueue.top();
				outputqueue.pop();	
				OActive->OCount ++;
			}
		}
		//if there is a process assigned to output then increment the  counter
		if ( OActive != NULL){
			if (OActive->IOTimer < OActive->history[OActive->sub].second)
			{
			       OActive-> IOTimer ++;
			       OActive-> OTotal ++;
			}
			else
			{
			       OActive -> IOTimer   = 0;
			       OActive -> sub ++ ;
			       readyqueue.push(OActive);
			       OActive = NULL;
			}
		}
	}		

			//print the whole status of the program and print detils when run ends
			cout<<"The run has ended."<<endl
			<<"The final value of the timer was:  "<<i<<endl
			<<"The amount of time spent idle was:  "<<idlecounter<<endl
			<<"Number of terminated processes = "<<terminatedcounter<<endl;
			if (Active == NULL)
				cout<<"Active is 0"<<endl;
			else
				cout<<"Active is "<<Active-> processId<<endl;
			if (IActive == NULL)
				cout<<"IActive is 0"<<endl;
			else
				cout<<"IActive is "<<IActive->processId<<endl;
			if (OActive == NULL)
				cout<<"OActive is 0"<<endl;
			else
				cout<<"OActive is "<<OActive->processId<<endl;
			cout<<"Contents of the Entry Queue:"<<endl;
			showpidentry(entryqueue);    
			cout<<"Contents of the Ready Queue:"<<endl;
			showpid(readyqueue);  
			cout<<"Contents of the Input Queue:"<<endl;
			showpid(inputqueue);
			cout<<"Contents of the Output Queue:"<<endl;
			showpid(inputqueue);

	return 0;
}
//method to show the entry queue
void showpidentry (const queue<Process*> eq){
	Process* p;
	if (eq.empty())
		cout<<"(empty)";
	else
		for (queue<Process*> dump = eq; !dump.empty(); dump.pop())
		{	p = dump.front();
       	 	cout << p->getprocessId()<<" ";	
		}
	cout<<endl<<endl;
}
//method to show the process if of the given priority queue

void showpid (const priority_queue<Process*,deque<Process*>, Compare > q){
	Process* p;
	if (q.empty())
		cout<<"(empty)";
	else
		for (priority_queue<Process*,deque<Process*>, Compare >  dump = q; !dump.empty(); dump.pop())
	{	p = dump.top();
        cout << p->getprocessId()<<" ";	
	}
	cout<<endl<<endl<<endl;
}

