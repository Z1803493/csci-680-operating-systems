/****************************************************************
CSCI 480/580 - Assignment 4 - Semester (Fall) 2016
Program:        Assign4.c
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Monday, October 10, 2016
Purpose:        This works with the shell implementation of the 
				command and read output.
****************************************************************/
#include "Assign4.h"

void main(void)
{
    char* s;

    char line[1024];
    char* argv[64] = { 0 };
    while (1) {
        printf("\n480Shell : ");
        fgets(line, 1024, stdin);
        line[strlen(line) - 1] = 0;
        //printf("\n");
        //printf("%s\n", line);
        s = strstr(line, "||");
        if (s != NULL) {
            executePipe(line);
        }
        else {
            parse(line, argv);
            if (strcmp(argv[0], "quit") == 0 || strcmp(argv[0], "q") == 0)
                exit(0);
            execute(argv);
        }
    }
    return;
}

//takes as input the simgle command and work on it !


void parse(char* line, char** argv){

    while (*line != '\0') {
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';
        *argv++ = line;
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n')
            line++;
    }
    *argv = '\0';
}
//execute a single command
void execute(char** argv){
    pid_t pid;
    int status;
    if ((pid = fork()) < 0) { /* fork a child process           */
        perror(" error : forking child process failed");
        exit(1);
    }
    else if (pid == 0) { /* for the child process:         */
        if (execvp(*argv, argv) < 0) { /* execute the command  */
            perror("error: exec failed\n");
            exit(1);
        }
    }
    else { /* for the parent:      */
        while (wait(&status) != pid); /* wait for completion  */
    }
}

//take piped command as input and work on it !

void executePipe(char* str){
    int pipefd[2];
    int pid, status, i=0;
    char* command[4][1024];
    char* command_2[1024];
    char* command_1[1024];
    const char s[3] = "||";
    const char a[3] = " ";
    char* token;
    token = strtok(str, s);
    while (token != NULL && i < 4) {
        *command[i] = token;
        token = strtok(NULL, s);
        i++;
    }
    token = strtok(*command[0], a);
    while (token != NULL) {
        command_1[i] = token;
        token = strtok(NULL, a);
        i++;
    }
    i = 0;
    token = strtok(*command[1], a);
    while (token != NULL) {
        command_2[i] = token;
        token = strtok(NULL, a);
        i++;
    }
    pipe(pipefd);
    pid = fork();
    if (pid == 0) {
        dup2(pipefd[0], 0);
        close(pipefd[1]);
        //printf("%s", *command_2);
        execvp(command_2[0], command_2);
    }
    else {
        dup2(pipefd[1], 1);
        close(pipefd[0]); // close unput half of pipe
        //printf("%s", *command_1);
        execvp(command_1[0], command_1); // execute cat
        while(waitpid(&status) != pid); /* wait for completion  */
    }
    return;
}



