/****************************************************************
CSCI 480/580 - Assignment 4 - Semester (Fall) 2016
Program:        Assign4.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Monday, October 10, 2016
Purpose:        Header file for Shell implementation.
****************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

//execute a single command
void execute(char** argv);
//take piped command as input and work on it !
void executePipe(char* str);

//takes as input the simgle command and work on it !
void parse(char* line, char** argv);