/******************************************************
CSCI 480/580 - Assignment 1 - Semester (Fall) 2016
Program:        prog_a.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Saturday, September 3, 2016
Purpose:        show the Specs of the CPU & Time
*******************************************************/
#include "prog.h"

int main(){	
	int total;
	string line,w_s;
	ifstream utime("/proc/uptime"),cpuinfo("/proc/cpuinfo"); //input of the source
	string  processor = outputOnterminal("cat /proc/cpuinfo | grep processor | wc -l"); //for processer
	string  multicore = outputOnterminal("nproc --all"); //for multi core in linux
	cout<<"Hopper has "<<processor<<"processor"<<endl;
	cout<<"Hopper has "<<multicore<<"multicore chip"<<endl;     
	while( utime.good() )
	{
	    getline( utime, line);
		stringstream ss(line);
    	while(getline(ss,w_s,' '))
	    {
	     	total = atoi(w_s.c_str());
	      	break;
	    }
	}
	uptime(total);
	utime.close();
	return 0;
}
//Simple method to calculate the time
void uptime(int total){
	//Simple variable to store values
	int seconds,hours,minutes,days; 
	//Calcuate the specific values
	minutes = total /60.0;	
	seconds = total % 60;
	hours = minutes / 60.0;
	minutes = minutes % 60 ;
	days = hours / 24.0;
	hours = hours % 24; 
	cout<<"Hopper is up for  "<<total<<" Seconds"<<endl;
	cout<<days<<" days, "<< hours <<" hours, "<<minutes<<" minutes, "<<seconds<<" seconds "<<endl;
}

string outputOnterminal(string command) {  //A simple method to pull the command line output stream for a command
    string commandData;
    FILE * fileStream;	
    const int storage = 256;  //Buffer to store
    char storageBuffer[storage];	
    command.append(" 2>&1");
	fileStream = popen(command.c_str(), "r");
    if (fileStream) {
		while (!feof(fileStream))
    		if (fgets(storageBuffer, storage, fileStream) != NULL) 
    			commandData.append(storageBuffer);
    	pclose(fileStream);
    }
	return commandData; //return the Output of the command
}
