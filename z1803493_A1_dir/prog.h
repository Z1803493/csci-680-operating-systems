/******************************************************
CSCI 480/580 - Assignment 1 - Semester (Fall) 2016
Program:        prog.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Saturday, September 3, 2016
Purpose:        Header File for all the programs
*******************************************************/
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

string outputOnterminal(string );
void uptime(int total);