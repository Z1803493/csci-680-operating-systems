/******************************************************
CSCI 480/580 - Assignment 1 - Semester (Fall) 2016
Program:        prog_b.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Saturday, September 3, 2016
Purpose:        show the Specs of the 0 Processor
*******************************************************/
#include "prog.h"

string outputOnterminal(string command) {  //A simple method to pull the command line output stream for a command

    string commandData;
    FILE * fileStream;	
    const int storage = 256;  //Buffer to store
    char storageBuffer[storage];	
    command.append(" 2>&1");
	fileStream = popen(command.c_str(), "r");
    if (fileStream) {
		while (!feof(fileStream))
    		if (fgets(storageBuffer, storage, fileStream) != NULL) 
    			commandData.append(storageBuffer);
    	pclose(fileStream);
    }
	return commandData; //return the Output of the command
}

int main (){
    string  fpu     = outputOnterminal("grep \"fpu\" /proc/cpuinfo | uniq | head -1");
    string  vendor  = outputOnterminal("grep \"vendor_id\" /proc/cpuinfo | uniq | head -1");
    string  model   = outputOnterminal("grep \"model name\" /proc/cpuinfo | uniq | head -1");
    string  cpu_s   = outputOnterminal("grep \"cpu MHz\" /proc/cpuinfo | uniq | head -1");
    string  add_s   = outputOnterminal("grep \"address sizes\" /proc/cpuinfo | uniq | head -1");
    string  flags   = outputOnterminal("grep \"flags\" /proc/cpuinfo | uniq | head -1");

    cout<<"Hopper has "<<endl<<fpu<<vendor<<model<<cpu_s<<add_s<<flags<<endl;


    return 0;
}