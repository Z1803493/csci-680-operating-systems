/******************************************************
CSCI 480/580 - Assignment 1 - Semester (Fall) 2016
Program:        prog_c.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Saturday, September 3, 2016
Purpose:        show the Specs of the system
*******************************************************/
#include "prog.h"

string outputOnterminal(string command) {  //A simple method to pull the command line output stream for a command

    string commandData;
    FILE * fileStream;	
    const int storage = 256;  //Buffer to store
    char storageBuffer[storage];	
    command.append(" 2>&1");
	fileStream = popen(command.c_str(), "r");
    if (fileStream) {
		while (!feof(fileStream))
    		if (fgets(storageBuffer, storage, fileStream) != NULL) 
    			commandData.append(storageBuffer);
    	pclose(fileStream);
    }
	return commandData; //return the Output of the command
}

int main (){

	string  spec = outputOnterminal("cat /proc/sys/kernel/ostype && cat /proc/sys/kernel/osrelease && cat /proc/sys/kernel/hostname && cat /proc/sys/kernel/version  ");
	 cout<<"Hopper has "<<endl<<spec<<"as its Ostype, Osrelease, Hostname & Version "<<endl;     
	return 0;
}