/****************************************************************
CSCI 480/580 - Assignment 8  - Semester (Fall) 2016
Program:        Assign8.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Thursday, December 1, 2016
Purpose:        Implementation of the Random , FIFO, LRU algorithm
				for Paging   
****************************************************************/

#include <iostream>
#include <iomanip>
#include <vector>
#include <stdlib.h> 
#include <sstream>
#include <fstream>
using namespace std;


void print_vector(vector<int> v){
	for (auto i = v.begin(); i < v.end(); ++i)
			cout<<setw(10)<<*i;
}

void printh_header(int no_frame){
	cout<<setw(20)<<"Next";
	for (int i = 0; i < no_frame; ++i)
		cout<<setw(10)<<"Frame";
	cout<<endl<<setw(10)<<"Step"<<setw(10)<<"Page";
	for (int i = 0; i < no_frame; ++i)
		cout<<setw(10)<<i;
	cout<<setw(20)<<"Page Fault?"<<endl;
}
void print_row(int step,int pagen_no, vector<int> frame,int total_frame,int location){
	cout<<setw(10)<<step<<setw(10)<<pagen_no;
	print_vector(frame);
	for (int i = frame.size(); i < total_frame; ++i)	
			cout<<setw(10)<<" ";
	if (location == -1)
		cout<<setw(20)<<"No, Page Fault"<<endl;
	else
		cout<<setw(20)<< "Yes at frame  "<< location<<endl;
}

void fifo(int count_page,int count_frames, vector<int> page_numbers){
	int step = 0,counter =0;
	vector<int> page_table,frame_table,history;
	for (int i = 0; i < count_page; ++i)
		page_table.push_back(-1);

	cout<<"Simulation of FIFO algorithm for "<<count_page<<" Pages and "<<count_frames<<" Frames"<<endl;
	printh_header(count_frames);
	for(auto i = page_numbers.begin();i!=page_numbers.end();i++){
		int location  = -1;

		if ( page_table[*i] < 0 ){
				counter++;
			if(frame_table.size() < count_frames){
				location = frame_table.size();
				page_table[*i] = frame_table.size();
				history.push_back(step);				
				frame_table.push_back(*i);
			}
			else{
				int temp = 0;
				for (int i = 1; i < count_frames; ++i)
					if (history[i]<history[temp])
						temp = i;
				location = temp;
				page_table[frame_table[temp]] = -1;
				page_table[*i] = temp;
				frame_table[temp] = *i;
				history[temp] = step; 
			}
		}
		print_row(step,*i,frame_table,count_frames,location);
		step++;	
	}
	cout<<"In "<<step<<" steps, we had  "<<counter<<" page faults"<<endl<<endl;
}
void lru(int count_page,int count_frames, vector<int> page_numbers){
	int step = 0,counter = 0;
	vector<int> page_table,frame_table,history;
	for (int i = 0; i < count_page; ++i)
		page_table.push_back(-1);
	cout<<"Simulation of LRU algorithm for "<<count_page<<" Pages and "<<count_frames<<" Frames"<<endl;
	printh_header(count_frames);
	for(auto i = page_numbers.begin();i!=page_numbers.end();i++){
		int location  = -1;
		if ( page_table[*i] < 0 ){
			counter++;
			if(frame_table.size() < count_frames){
				location = frame_table.size();
				page_table[*i] = frame_table.size();
				history.push_back(step);				
				frame_table.push_back(*i);
			}
			else{
				int temp = 0;
				for (int i = 1; i < count_frames; ++i)
					if (history[i]<history[temp])
						temp = i;
				location = temp;
				page_table[frame_table[temp]] = -1;
				page_table[*i] = temp;
				frame_table[temp] = *i;
				history[temp] = step; 
			}
		}
		else
			history[page_table[*i]] = step; 
		print_row(step,page_numbers[*i],frame_table,count_frames,location);
		step++;	
	}
	cout<<"In "<<step<<" steps, we had  "<<counter<<" page faults"<<endl<<endl;
}
void random(int count_page,int count_frames, vector<int> page_numbers){
	int step = 0,counter = 0;
	vector<int> page_table,frame_table,history;
	for (int i = 0; i < count_page; ++i)
		page_table.push_back(-1);
	cout<<"Simulation of RANDOM algorithm for "<<count_page<<" Pages and "<<count_frames<<" Frames"<<endl;
	printh_header(count_frames);
	for(auto i = page_numbers.begin();i!=page_numbers.end();i++){
		int location  = -1;
		if ( page_table[*i] < 0 ){
			counter++;
			if(frame_table.size() < count_frames){
				location = frame_table.size();
				page_table[*i] = frame_table.size();
				history.push_back(step);				
				frame_table.push_back(*i);
			}
			else{
				int temp = rand()%count_frames;
				for (int i = 1; i < count_frames; ++i){
					if (history[i]<history[temp]){
						temp = i;
					}
				}
				location = temp;
				page_table[frame_table[temp]] = -1;
				page_table[*i] = temp;
				frame_table[temp] = *i;
				history[temp] = step; 
			}
		}
		print_row(step,page_numbers[*i],frame_table,count_frames,location);
		step++;	
	}
	cout<<"In "<<step<<" steps, we had  "<<counter<<" page faults"<<endl<<endl;
}


int main(){
	ifstream infile("data.txt");
	string input1;
	int i =1;
	for( string input1; getline( infile, input1 );){
		char mode;
		int count_page,count_frames,temp;
		string input2;
		vector<int> page_numbers;
		stringstream ss1(input1);
		ss1>>mode>>count_page>>count_frames;
		if(mode == '?')break;
		getline(infile,input2);
		stringstream ss2(input2);
		while( ss2>>temp && temp!= -1)
			page_numbers.push_back(temp);
		if ( mode == 'F')
			fifo(count_page,count_frames,page_numbers);
		else if (mode == 'L')
			lru(count_page,count_frames,page_numbers);
		else if (mode == 'R')
			random(count_page,count_frames,page_numbers);
		i++;
	}

	return 0;
}

