/****************************************************************
CSCI 480/580 - Assignment 7  - Semester (Fall) 2016
Program:        Assign7.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Wednesday, November 16, 2016
Purpose:        Header file to implement Mutex Threads and Semaphores  
****************************************************************/

#ifndef PROG_H
#define PROG_H

#include <iostream>
#include <pthread.h>
#include <vector>
#include <cstdlib>
#include <unistd.h>
#include <semaphore.h>
#include <string>
#include <queue>

using namespace std;

const int BUFFER_SIZE = 12;
const int P_NUMBER = 8;
const int C_NUMBER = 4;
const int N_P_STEPS = 4;
const int N_C_STEPS = 8;

void InitializeData();
void* Produce(void*);
void* Consume(void*);
void insertWidget(const string&, int);
void removeWidget(int);

int iCounter;
pthread_mutex_t mutex;
sem_t NotFull, NotEmpty;

queue<string> buffer;

#endif