/****************************************************************
CSCI 480/580 - Assignment 7  - Semester (Fall) 2016
Program:        Assign7.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Wednesday, November 16, 2016
Purpose:        Implementation file for Mutex Threads and Semaphores  
****************************************************************/


#include "Assign7.h"

int main(int argc, char* argv[])
{
    vector<pthread_t> v;
    //Initialize the data
    InitializeData();
    pthread_t ProducerThread, ConsumerThread;

    int* producer = new int[P_NUMBER];

    for (int i = 0; i < P_NUMBER; i++) {
        producer[i] = i;
        pthread_t t;
        //Threads creation
        if (pthread_create(&t, NULL, Produce, &producer[i]))
            cerr << "Error in pthread create" << endl;
        v.push_back(t);
        cout << "Creating Producer" << i << endl;
    }

    int* consumer = new int[C_NUMBER];
    for (int i = 0; i < C_NUMBER; i++) {
        consumer[i] = i;
        pthread_t t;
        if (pthread_create(&t, NULL, Consume, &consumer[i]))
            cerr << "Error in pthread create" << endl;
        v.push_back(t);
        cout << "Creating Consumer" << i << endl;
    }
    //Implementation of the joinning of all the threads 
    for (auto i = v.begin(); i < v.end(); ++i)
        pthread_join(*i, NULL);
    //destroy mutex
    if (pthread_mutex_destroy(&mutex))
        cerr << "Error in mutex destroy" << endl;
    //destroy semaphore
    if (sem_destroy(&NotFull))
        cerr << "Error in semaphore sem_destroy" << endl;
    //destroy semaphore
    if (sem_destroy(&NotEmpty))
        cerr << "Error in semaphore sem_destroy" << endl;
    return 0;
}

void InitializeData()
{
    //initilization 
    if (pthread_mutex_init(&mutex, NULL))
        cerr << "Error in mutex InitializeData" << endl;
    //semaphore for notempty
    if (sem_init(&NotEmpty, 0, 0))
        cerr << "Error in semaphore inti" << endl;
    //semaphore for not full
    if (sem_init(&NotFull, 0, BUFFER_SIZE))
        cerr << "Error in semaphore" << endl;
}

//method for production
void* Produce(void* Param)
{
    string widget;
    for (int i = 0; i < N_P_STEPS; ++i) {
        widget = "A0";
        widget[0] += *((int*)Param);
        widget[1] += i;
        //semaphore waiting 
        if (sem_wait(&NotFull))
            cerr << "Error in semaphore" << endl;
        //locking mutex
        if (pthread_mutex_lock(&mutex))
            cerr << "Error in mutex" << endl;
        //insert
        insertWidget(widget, *((int*)Param));
        //mutex unlocking
        if (pthread_mutex_unlock(&mutex))
            cerr << "Error in mutex" << endl;
        //post semaphore
        if (sem_post(&NotEmpty))
            cerr << "Error in semaphore" << endl;
        sleep(1);
    }
}
//method for consuming widget
void* Consume(void* Param)
{

    for (int i = 0; i < N_C_STEPS; ++i) {
        //semaphore for on empty
        if (sem_wait(&NotEmpty))
            cerr << "Error in semaphore" << endl;
        //mutex locking
        if (pthread_mutex_lock(&mutex))
            cerr << "Error in mutex" << endl;
        //removing widget
        removeWidget(*((int*)Param));
        //mutex unlocking
        if (pthread_mutex_unlock(&mutex))
            cerr << "Error in mutex" << endl;
        //semaphore not full
        if (sem_post(&NotFull))
            cerr << "Error in semaphore" << endl;
        sleep(1);
    }
}
//method for inserting into queue
void insertWidget(const string& widget, int Param)
{
    buffer.push(widget);
    queue<string> temp = buffer;
    cout << "Producer " << Param << " inserted one item.  Total is now " << buffer.size() << endl;
    cout << "Content of Buffer :" << endl;
    while (!temp.empty()) {
        cout << temp.front() << " ";
        temp.pop();
    }
    cout << endl
         << endl;
}
//method for removing widget form queue
void removeWidget(int Param)
{
    queue<string> temp = buffer;

    string widget = buffer.front();
    buffer.pop();
    cout << "Consumer " << Param << " removed one item.   Total is now " << buffer.size() << endl;
    cout << "Content of Buffer :" << endl;
    while (!temp.empty()) {
        cout << temp.front() << " ";
        temp.pop();
    }
    cout << endl
         << endl;
}
