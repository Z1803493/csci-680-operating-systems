/****************************************************************
CSCI 480/580 - Assignment  - Semester (Fall) 2016
Program:        Assign6.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Saturday, November 5, 2016
Purpose:        program to implement the Memory managment  
****************************************************************/

#include "Assign6.h"


void print(list<Memory> temp){
	if (temp.empty()){
		cout<<"(none)"<<endl<<"Total size of the list = 0"<<endl;
		return;
	}
	int total_size = 0;
	list<Memory>::iterator i;
	cout<<"Start Address\tSize"<<endl;
	for (i = temp.begin(); i != temp.end(); ++i){
		cout<<i->getstarting_address()<<" \t"<<i->getsize()<<endl;
		total_size = total_size + i->getsize();			
	}
	cout<<"Total size of the list = "<<total_size<<endl;

}


void Load(string input){
	char operation;
	int size;
	string pid,bid;
	stringstream s(input);
	s>>operation>>pid>>size>>bid;
	list<Memory>::iterator fit = Avail.end();
	list<Memory>::iterator temp;

	cout<<"Transaction:  request to load process"<<pid<<" of size  "<<size
		<<" bytes , block id : "<<bid<<endl;


	if ( MODE == 'B')
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
			if ( temp->getsize() >=  size && (fit == Avail.end() || temp->getsize() < fit->getsize())){
				fit = temp;
				cout<<"Found a block of size "<<temp->getsize() <<endl<<cout<<"Success in allocating a block"<<endl;}
	else 
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
			if ( temp->getsize() >=  size ){
				fit = temp;
				cout<<"Found a block of size "<<temp->getsize() <<endl<<cout<<"Success in allocating a block"<<endl;
				break;
			}

	if ( fit == Avail.end()){
		cout<<"Unable to comply as no block of adequate size is available"<<endl;
		return ;
	}
	
	Memory mem_temp(fit->getstarting_address(), size, pid, bid);
	InUse.push_back(mem_temp);
	fit->setstarting_address(fit->getstarting_address() + size);
	fit->setsize(fit->getsize() - size);
	

	if ( fit->getsize() == 0)
		Avail.erase(fit);
}

void Allocate(string input){
	char operation;
	int size;
	string pid,bid;
	stringstream s(input);
	s>>operation>>pid>>size>>bid;
	
	cout<<"Transaction:  request to Allocate "<<size<<"bytes of data for process"<<pid<<", Block id "<<bid<<endl;

list<Memory>::iterator fit = Avail.end();
	list<Memory>::iterator temp;

	if ( MODE == 'B'){
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
		{
			if ( temp->getsize() >=  size && (fit == Avail.end() || temp->getsize() < fit->getsize()))
			{
				fit = temp;
				cout<<"Found a block of size "<<temp->getsize() <<endl<<"Success in allocating a block"<<endl;

				
			}
		}
		
	}
	else 
	{
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
		{
			if ( temp->getsize() >=  size )
			{
				fit = temp;
				cout<<"Found a block of size "<<temp->getsize() <<endl<<"Success in allocating a block"<<endl;
				break;
				
			}
		}
	}

	if ( fit == Avail.end())
	{
		cout<<"Unable to comply as the  process could not be found."<<endl;
		return ;
	}
	
	Memory mem_temp(fit->getstarting_address(), size, pid, bid);
	InUse.push_back(mem_temp);
	fit->setstarting_address(fit->getstarting_address() + size);
	fit->setsize(fit->getsize() - size);

	if ( fit->getsize() == 0)
	{
		Avail.erase(fit);
	}


}
void Deallocate(string input){
	char operation;
	string pid, bid;
	stringstream s(input);
	s>>operation>> pid>>bid;
	cout<<"Transaction:  request to Deallocate block id : "<<bid<<" from  process id : "<<pid<<endl;
	list<Memory>::iterator temp;
	list<Memory>::iterator temp2;
	for ( temp = InUse.begin() ; temp != InUse.end(); ++temp)
		{
			if ( temp->getprocess_id() == pid && temp -> getblock_id() == bid )
			{
				break;
			}

		}
		if ( temp == InUse.end() )
		{
				cout<<"Unable to comply as the indicated block cannot be found. "<<endl;
			return;
		}
	for ( temp2 = Avail.begin() ; temp2 != Avail.end(); ++temp2)
		{
			if ( temp2->getstarting_address() > temp->getstarting_address() )
			{
				Avail.insert(temp2,*temp);
				break;
			}
		}
		if ( temp2 == Avail.end() )
		{
			Avail.push_back(*temp);
		}
		InUse.erase(temp);
		//
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
		{
			temp2 = temp;
			temp2++;
			while ( temp2!= Avail.end() && 
			 temp-> getstarting_address() + temp->getsize() == temp2-> getstarting_address()
			 && temp->getsize() + temp2->getsize() <= 4*MB 
			 )
			{
				cout<<"Merging two blocks at "<<temp->getstarting_address()<<" and "<<temp2 ->getstarting_address()<<endl;
				temp->setsize(temp->getsize() + temp2 ->getsize()) ;

				temp2 = Avail.erase(temp2);
			}

		}

		cout<<"Success in deallocating a block"<<endl;

}
void Terminate(string input){
	char operation;
	string pid;
	stringstream s(input);
	s>>operation>>pid;
	cout<<"Terminate process"<<endl<<" process id : "<<pid<<endl;
	list<Memory>::iterator temp;
	list<Memory>::iterator temp2;
	list<Memory>::iterator temp3;

	for ( temp = InUse.begin() ; temp != InUse.end(); ++temp)
		{
				if ( temp->getprocess_id() == pid )
				{
					
					for ( temp2 = Avail.begin() ; temp2 != Avail.end(); ++temp2)
					{
						if ( temp2->getstarting_address() > temp->getstarting_address() )
						{
							Avail.insert(temp2,*temp);
							break;
						}
					}
					if ( temp2 == Avail.end() )
					{
						Avail.push_back(*temp);
					}
					temp = InUse.erase(temp);

				}


		}
		for ( temp = Avail.begin() ; temp != Avail.end(); ++temp)
		{
			temp2 = temp;
			temp2++;
			while ( temp2!= Avail.end() && 
			 temp-> getstarting_address() + temp->getsize() == temp2-> getstarting_address()
			 && temp->getsize() + temp2->getsize() <= 4*MB 
			 )
			{
				cout<<"Merging two blocks at "<<temp->getstarting_address()<<" and "<<temp2 ->getstarting_address()<<endl;
				temp->setsize(temp->getsize() + temp2 ->getsize()) ;
				temp2 = Avail.erase(temp2);
			}

		}
		cout<<"Success in Terminating a process"<<endl;
}

int main(int argc, char* argv[])
{
	int i = 0 ;
	
	int pid,address;
	ifstream infile("data6.txt");
	string line,bid;
	Memory 	m1(START,MB),
			m2(START + MB  ,2*MB),
			m3(START + 3*MB,2*MB),
			m4(START + 5*MB,4*MB),
			m5(START + 9*MB,4*MB);

	
	Avail.push_back(m1);
	Avail.push_back(m2);
	Avail.push_back(m3);
	Avail.push_back(m4);
	Avail.push_back(m5);
	MODE = *argv[1];
	if (MODE == 'B')
	{
		cout<<"Beginning of the program"<<endl
			<<"Simulation of Memory Management using the Best-Fit algorithm"<<endl
			<<"Beginning of the run"<<endl;
	}
	
	if ( MODE == 'F')
	{
		cout<<"Beginning of the program"<<endl
			<<"Simulation of Memory Management using the First-Fit algorithm"<<endl
			<<"Beginning of the run"<<endl;
	}

	cout<<"List of available blocks"<<endl<<endl;
	print(Avail);
	cout<<"List of blocks in use"<<endl<<endl;
	print(InUse);
	
	while( infile.good()){
		getline(infile,line);
		switch(line[0]){
			case 'L': Load(line); 
						cout<<endl<<endl<<"List of available blocks"<<endl;
						print(Avail);
						cout<<"List of blocks in use"<<endl;
						print(InUse);
						break;
			case 'T': Terminate(line); 
						cout<<endl<<endl<<"List of available blocks"<<endl;
						print(Avail);
						cout<<"List of blocks in use"<<endl;
						print(InUse);
						break;
			case 'A': Allocate(line); 
						cout<<endl<<endl<<"List of available blocks"<<endl;
						print(Avail);
						cout<<"List of blocks in use"<<endl;
						print(InUse);
						break;
			case 'D': Deallocate(line); 
						cout<<endl<<endl<<"List of available blocks"<<endl;
						print(Avail);
						cout<<"List of blocks in use"<<endl;
						print(InUse);
						break;
			case '?': goto REDIRECT;break;
		}
		i++;
	}

	REDIRECT:	cout<<endl<<"End of the run"<<endl<<endl<<"List of available blocks"<<endl;
	print(Avail);
	cout<<"List of blocks in use"<<endl;
	print(InUse);

	

	return 0;
}