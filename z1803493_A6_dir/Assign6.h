/****************************************************************
CSCI 480/580 - Assignment  - Semester (Fall) 2016
Program:        Assign6.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        2
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Saturday, November 5, 2016
Purpose:        Header file to implement the Memory managment  
****************************************************************/

#ifndef PROG_H
#define PROG_H


#include <iostream>
#include <list>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
using namespace std;
const int MB = 1048576;
const int START = 3145728;
const int HOWOFTEN = 5;

char MODE ; 

class Memory
{

	int starting_address, size;
	string process_id,block_id;
public:	
	Memory(int sa=0, int sz=0, string pid="",string bid="" ):
	starting_address(sa),size(sz),process_id(pid),block_id(bid){

	}
	~Memory(){}
	void setstarting_address(int sa){starting_address = sa;}	
	void setsize(int sz){size = sz;}	
	void setprocess_id(string pid){process_id = pid;}
	void setblock_id(string bid){block_id = bid;}
	int getstarting_address(){return starting_address;}
	int getsize(){return size;}
	string getprocess_id(){return process_id;}
	string getblock_id(){return block_id;}
};

list<Memory> InUse, Avail;

#endif