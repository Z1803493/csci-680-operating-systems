/******************************************************
CSCI 480/580 - Assignment 3 - Semester (Fall) 2016
Program:        Assign3.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Sunday, September 25, 2016
Purpose:        Header File the programs

*******************************************************/
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;

void Pwork (int i);
void Cwork (int i);
void Gwork (int i);
