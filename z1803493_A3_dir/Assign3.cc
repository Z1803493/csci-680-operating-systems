/****************************************************************
CSCI 480/580 - Assignment 3 - Semester (Fall) 2016
Program:        Assign3.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Harry Hutchins.
TA:             Brian Kirk
TA:             Mark White
Date Due:       Sunday, September 25, 2016
Purpose:        This demostrated the interprocess communication
                so that the buffer is shared and processes communicate
****************************************************************/

#include "Assign3.h"

int PipeA_first[2],PipeB_second[2],PipeC_third[2],c=0;char Input_Buffer[256],Output_Buffer[256];pid_t CP,GCP; 
/****************************************************************
This method is called by child
***************************************************************/
void PWork(int c){

    if (c == 0){
        int M;
        M = 1;
        write(PipeA_first[1], &M, 1);
        cout << "Message " << c << " from process Parent:  "<< "Value = " << M << '\n';
        close(PipeA_first[1]);
    }
    else {
        int M;
        close(PipeC_third[1]);
        read(PipeC_third[0], &M, 1);
        close(PipeC_third[0]);
        M = 2 * M + 3;
        cout << "Message " << c << " from process " << left << setw(15) << "Parent :"<< "Value = " << M << '\n';
        write(PipeA_first[1], &M, 1);
        close(PipeA_first[1]);
    }
}

/****************************************************************
This is a method called by child
****************************************************************/
void CWork(int c){
    int M;
    close(PipeA_first[1]);
    read(PipeA_first[0], &M, 1);
    close(PipeA_first[0]);
    M = 2 * M + 3;
    cout << "Message " << c << " from process" << left << setw(15) << " Child :"<< " Value = " << M << '\n';
    write(PipeB_second[1], &M, 1);
    close(PipeB_second[1]);
}
/*******************************************************************************
This is a method called by grand child 
*******************************************************************************/
void GWork(int c){
    int M;
    close(PipeB_second[1]);
    read(PipeB_second[0], &M, 1);
    close(PipeB_second[0]);
    M = 2 * M + 3;
    cout << "Message " << c << " from process" << left << setw(15) << " GrandChild :"<< " Value = " << M << '\n';
    write(PipeC_third[1], &M, 1);
    close(PipeC_third[1]);
}
/*******************************************************************************
This main funciton Creates a processes and child and also grand hcild for processes .
*******************************************************************************/
int main(int argc, char const *argv[]){
    int Piper_Status, GrandChild_Process_state = -1; 
    Piper_Status = pipe(PipeA_first); 
    if (Piper_Status == -1) {
        cerr << "Error in creating pipe";
        exit(-5);
    }
    Piper_Status = pipe(PipeB_second);
    if (Piper_Status == -1){    
        cerr << "Error in creating pipe";
        exit(-5);
    }
    Piper_Status = pipe(PipeC_third);
    if (Piper_Status == -1) {
        cerr << "Error in creating pipe";
        exit(-5);
    }
    PWork(c);CP = fork(); 
    if (CP == -1){
        cerr << "Error in creating fork";
        exit(-1);
    }
    for (c = 1; c <= 8; c++) {
        if (c > 8) {
            wait(0);
            exit(-1);
        }
        if (CP == 0) {
            CWork(c);
            if (c == 0) GCP = fork();
            if (c > 8) {
                wait(0);
                exit(-1);
            }
            if (GCP == -1) {
                cerr << "Error in creating fork";
                exit(-1);
            }else if (GCP == 0) {
                GWork(c);
                GrandChild_Process_state = 1;
                if (c > 8) exit(-1);
            }
        }
        if (GrandChild_Process_state == 1) PWork(c);
    }
    return 0;
}