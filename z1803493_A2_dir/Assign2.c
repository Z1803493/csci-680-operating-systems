/******************************************************
CSCI 480/580 - Assignment 2 - Semester (Fall) 2016
Program:        Assign2.c
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Tuesday, September 13, 2016
Purpose:       	Program stating process manuolation using
				fork, wait, sleep, getpid, getppid etc
*******************************************************/
#include "Assign2.h"

int main(int argc, char const* argv[])
{
    setbuf(stdout, NULL);
    printf("\nI'm the original process. My PID is %d and my parent\'s PID is %d \n", getpid(), getppid());
    printf("Now we have the first fork.\n");

    //calling fork()
    pid_t cPid_Zero = fork();

    //check for fork() fail case
    if (cPid_Zero < 0) {
        //when fork fails messages to give to stderr
        fprintf(stderr, "The first fork failed\n");
        exit(-1);
    }
    //parent Process
    else if (cPid_Zero != 0) {

        printf("I am the parent.  My PID is  %d  and my child\'s PID is  %d\n", getpid(), cPid_Zero);
        printf("It is about to call ps\n");
        //Call the sleep() function to sleep for 2 seconds.
        sleep(2);
        //Print a message saying it is about to call ps.
        printf("I am the parent, about to call ps.\n");
        //Use system() to execute the "ps" command.
        system("ps");
        //Use wait(0) to wait for the child to terminate.
        wait(0);
        //Print a message saying it is about to terminate.
        printf("I am the parent, about to exit.\n");
        exit(0);
    }
    //child Process created by fork()
    else {
        printf("I am the child.  My PID is  %d  and my parent\'s PID is  %d\n", getpid(), getppid());
        printf("Now we have the second fork.\n");

        //call fork() second time
        pid_t gcPid_Zero = fork();
        //check for fork() fail case
        if (gcPid_Zero < 0) {
            fprintf(stderr, "The second fork failed\n");
            exit(-1);
        }
        //child as parent Process for grand child Process
        else if (gcPid_Zero != 0) {
            printf("I am the Child.  My PID is  %d  and my parent\'s PID is  %d\n", getpid(), getppid());
            printf("It is about to call ps\n");
            //Use wait(0) to wait for the grandchild to terminate.
            wait(0);
            //Print a message saying it is about to exit.
            printf("I am the child, about to exit.\n");
            //Exit with a status of 0.
            exit(0);
        }
        // grand child process by second fork()
        else {
            printf("I am the Grand child.  My PID is  %d  and my parent\'s PID is  %d\n", getpid(), getppid());
            //Print a message saying it is about to exit.
            printf("I am the grandchild, about to exit.\n");
            //Exit with a status of 0.
            exit(0);
        }
    }
    return 0;
}