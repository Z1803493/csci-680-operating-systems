/******************************************************
CSCI 480/580 - Assignment 2 - Semester (Fall) 2016
Program:        Assign2.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:		Harry Hutchins.
TA:             Brian Kirk
TA: 			Mark White
Date Due:       Sunday, September 25, 2016
Purpose:        Header File the programs
*******************************************************/
#include <stdio.h>		//header file for standard input and output
#include <stdlib.h>		//header file for sleep, fork,getpid, getppid function
#include <unistd.h>		//header file for exit and system function
#include <sys/wait.h> 	// header file which for wait function